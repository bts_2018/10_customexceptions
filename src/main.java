
public class main {

	public static void main(String[] args) {

		//CASE 1
		try{
			int age = 17 ;

			if(age<18) {
				throw new Exception("Driver is too young"); 
			}
			if(age>68) {
				throw new Exception("Driver is too old"); 
			}	

			System.out.println("can drive") ;
		}
		catch(Exception exp){

			System.out.println("Error: " + exp.getMessage()) ;
		}


		//CASE 2
		try{

			throw new MyException("This is my custom error message");
		}
		catch(MyException exp){

		}

	}

}
